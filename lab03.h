#pragma once
#include <iostream>
#include <string>

#include "cryptlib.h"
#include "osrng.h"
#include "secblock.h"
#include "files.h"
#include "hex.h"
#include "modes.h"

namespace lab3 {

	void KeyGen(CryptoPP::SecByteBlock& key)
	{
		CryptoPP::AutoSeededRandomPool prng;
		prng.GenerateBlock(key, key.size());
	}

	void Sign(CryptoPP::SecByteBlock& key, std::string& plaintext, std::string& mac, std::string& hmacText)
	{
		// HMAC
		CryptoPP::HMAC< CryptoPP::SHA256> hmac(key, key.size());

		// Perform the signing
		CryptoPP::StringSource ss2(plaintext, true,
			new  CryptoPP::HashFilter(hmac,
				new  CryptoPP::StringSink(mac)));

		CryptoPP::StringSource ss3(mac, true,
			new  CryptoPP::HexEncoder(
				new  CryptoPP::StringSink(hmacText)));
	}

	bool Verify(CryptoPP::SecByteBlock& key, std::string& plainText, std::string& mac)
	{
		try
		{
			// HMAC
			CryptoPP::HMAC< CryptoPP::SHA256> hmac(key, key.size());
			const int flags = CryptoPP::HashVerificationFilter::THROW_EXCEPTION | CryptoPP::HashVerificationFilter::HASH_AT_END;

			// Perform a verification
			CryptoPP::StringSource sss(plainText + mac, true,
				new  CryptoPP::HashVerificationFilter(hmac, NULL, flags));

			return true;
		}
		catch (const CryptoPP::Exception & e)
		{
			return false;
		}

	}


	void lab3()
	{
		CryptoPP::SecByteBlock key(32);
		KeyGen(key);
		std::string plainText = "some random text from lab03", mac, hmacText;

		Sign(key, plainText, mac, hmacText);


		std::cout << "PlainText: " << plainText << std::endl;

		std::cout << "HMAC: " << hmacText << std::endl;

		mac[0] = 'x';

		if (Verify(key, plainText, mac))
		{
			std::cout << "Verified message" << std::endl;
		}
		else
		{
			std::cout << "Unverified message" << std::endl;
		}


	}
}