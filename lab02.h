#pragma once
#include <iostream>
#include <string>
#include "modes.h"
#include "osrng.h"
#include "secblock.h"
#include "files.h"
#include "hex.h"

using namespace CryptoPP;

namespace lab2 {
	void KeyGen(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv)
	{
		AutoSeededRandomPool prng;
		prng.GenerateBlock(key, key.size());
		prng.GenerateBlock(iv, iv.size());
	}

	void Enc(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv, std::string& plaintext, std::string& ciphertext)
	{
		// Encryption object
		CTR_Mode<AES>::Encryption enc;
		enc.SetKeyWithIV(key, key.size(), iv, iv.size());

		// Perform the encryption
		ciphertext.resize(plaintext.size());
		enc.ProcessData((byte*)&ciphertext[0], (const byte*)plaintext.data(), plaintext.size());
	}

	void Dec(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv, std::string& ciphertext, std::string& decrypted)
	{
		// Decryption object
		CTR_Mode<AES>::Decryption dec;
		dec.SetKeyWithIV(key, key.size(), iv, iv.size());

		// Perform the decryption
		decrypted.resize(ciphertext.size());
		dec.ProcessData((byte*)&decrypted[0], (const byte*)ciphertext.data(), ciphertext.size());
	}

	void lab2()
	{
		std::string plaintext = "some random text  from lab02", ciphertext, decrypted;

		SecByteBlock key(32), iv(16);
		KeyGen(key, iv);

		std::cout << "Plaintext: " << plaintext << std::endl;

		Enc(key, iv, plaintext, ciphertext);

		std::cout << "Ciphertext: " << ciphertext << std::endl;

		Dec(key, iv, ciphertext, decrypted);

		std::cout << "Decrypted: " << decrypted << std::endl;

	}
}