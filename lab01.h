#pragma once
#include <iostream>
#include <string>

#include "cryptlib.h"
#include "secblock.h"
#include "chacha.h"
#include "osrng.h"
#include "files.h"
#include "hex.h"


namespace lab1 {

	void KeyGen(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv)
	{
		CryptoPP::AutoSeededRandomPool prng;
		prng.GenerateBlock(key, key.size());
		prng.GenerateBlock(iv, iv.size());
	}

	void Enc(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv, std::string& plaintext, std::string& ciphertext)
	{
		// Encryption object
		CryptoPP::ChaCha::Encryption enc;
		enc.SetKeyWithIV(key, key.size(), iv, iv.size());

		// Perform the encryption
		ciphertext.resize(plaintext.size());
		enc.ProcessData((CryptoPP::byte*) & ciphertext[0], (const CryptoPP::byte*)plaintext.data(), plaintext.size());
	}

	void Dec(CryptoPP::SecByteBlock& key, CryptoPP::SecByteBlock& iv, std::string& ciphertext, std::string& decrypted)
	{
		// Decryption object
		CryptoPP::ChaCha::Decryption dec;
		dec.SetKeyWithIV(key, key.size(), iv, iv.size());

		// Perform the decryption
		decrypted.resize(ciphertext.size());
		dec.ProcessData((CryptoPP::byte*) & decrypted[0], (const CryptoPP::byte*)ciphertext.data(), ciphertext.size());
	}

	void lab1()
	{
		std::string plaintext = "some plaintext from lab01", ciphertext, decrypted;

		CryptoPP::SecByteBlock key(32), iv(8);
		KeyGen(key, iv);

		std::cout << "Plaintext: " << plaintext << std::endl;

		Enc(key, iv, plaintext, ciphertext);

		std::cout << "Ciphertext: " << ciphertext << std::endl;

		Dec(key, iv, ciphertext, decrypted);

		std::cout << "Decrypted: " << decrypted << std::endl;

	}
}
